# Curso del lenguaje Dart

## Lecciones:

* [Inicio del curso y presentación](#module01)
    * [Lección #1 - Presentación del curso y objetivos](#lesson01)
    * [Lección #2 - El lenguaje de programación DART](#lesson02)
    * [Lección #3 - Preparar el IDE](#lesson03)
    * [Lección #4 - Crear y ejecutar primer proyecto](#lesson04)
    * [Lección #5 - Conocimientos previos](#lesson05)
    * [Lección #6 - Variables](#lesson06)
    * [Lección #7 - Números](#lesson07)
    * [Lección #8 - Operadores](#lesson08)
    * [Lección #9 - Constantes, final y boolean](#lesson09)
    * [Lección #10 - Cadenas](#lesson10)
* [Condicionales y ciclos](#module02)
    * [Lección #11 - Condicional IF y operadores](#lesson11)
    * [Lección #12 - Ciclo for](#lesson12)
    * [Lección #13 - While y Do While](#lesson13)
    * [Lección #14 - Switch](#lesson14)
* [Funciones en Dart](#module03)
    * [Lección #15 - Funciones](#lesson15)
    * [Lección #16 - Retorno de elementos](#lesson16)
    * [Lección #17 - Parámetros en funciones](#lesson17)
    * [Lección #18 - Parámetros opcionales](#lesson18)
* [Programación orientada a objetos](#module04)
    * [Lección #19 - Clases](#lesson19)
    * [Lección #20 - Métodos](#lesson20)
    * [Lección #21 - Constructor](#lesson21)
    * [Lección #22 - Tipos de constructores](#lesson22)
    * [Lección #23 - Set y Get](#lesson23)
    * [Lección #24 - Herencia en Dart](#lesson24)
    * [Lección #25 - Sobre escribir métodos](#lesson25)
    * [Lección #26 - Herencia y constructor](#lesson26)
    * [Lección #27 - Clases abstractas](#lesson27)
    * [Lección #28 - Interfaces](#lesson28)
* [Estructura de datos e imports](#module05)
    * [Lección #29 - Listas](#lesson29)
    * [Lección #30 - Listas de objetos](#lesson30)
    * [Lección #31 - Maps](#lesson31)
    * [Lección #32 - Imports](#lesson32)

## <a name="module01"></a> Inicio del curso y presentación

### <a name="lesson01"></a> Lección #1 - Presentación del curso y objetivos
- - -
Curso previo a Flutter para compreder el lenguaje de programación Dart.

### <a name="lesson02"></a> Lección #2 - El lenguaje de programación DART
- - -
Fue inventado en el 2011 por el equipo de Google y se le considera una alternativa a JavaScript. Trabaja en máquinas virtuales aunque puede trabajar en Google Chrome. En el 2013 fue liberada la versión estable.

Hoy en día va por la versión ^2.0 y se recomienda trabajar con esta versión. Para usarlo debemos bajar su SDK pero en Chromium ya podemos trabajar de una vez con la máquina virtual de Dart y otros elementos.

Dart tiene influencias de Java, Go y JavaScript.

### <a name="lesson03"></a> Lección #3 - Preparar el IDE
- - -
Podemos consultar la documentación oficial de Dart en https://dart.dev/

Dart no solo sirve para crear aplicaciones mobile con Flutter, también podemos crear aplicaciones web o scripts en el servidor como lo indican en https://dart.dev/tools. Como nos estamos centrando en Dart usaremos el SDK (web).

En este curso podemos trabajar con Android Studio, Visual Studio Code o cualquier IDE de JetBrains. Es importante instalar el plugin de DART (Normalmente en JetBrains ya vienen instalados) y podemos comprobarlo desde su menú de bienvenida cuando queremos crear un proyecto, sin embargo hace falta instalar el SDK para poder crear los proyectos.

Para instalar el SDK de Dart podemos acceder en https://dart.dev/get-dart y debemos seguir los pasos que nos indican.

### <a name="lesson04"></a> Lección #4 - Crear y ejecutar primer proyecto
- - -
En esta lección vamos a crear un proyecto en Dart usando JetBrains y escogeremos la opción **Dart Package - A starting point for Dart libraries or applications**, le daremos a siguiente y escogeremos la carpeta donde desearemos crearla.

*Nota: En JetBrains al darle click en Project, podemos ver las "perspectivas del proyecto"... Es decir, si queremos ver todos los archivos le damos click a Project Files, si queremos ver solo los archivos de pruebas le damos a Tests, etc.*

Si nos fijamos en el proyecto tendremos tres archivos muy similares unos con los otros que son:
* example/curso_del_lenguaje_dart_example.dart
* lib/src/curso_del_lenguaje_dart_base.dart
* lib/curso_del_lenguaje_dart.dart

Pero de ellos hablaremos más adelante. Para correr el proyecto con JetBrains vamos a Run -> Run... Escogeremos el proyecto y debe salir una ventana abajo con el mensaje *awesome: true*. Si queremos escribir nuestro primer hola mundo debemos editar nuestra función principal de example y le damos a correr de nuevo.

```dart
void main() {
  print('¡Hola mundo!');
}
```

### <a name="lesson05"></a> Lección #5 - Conocimientos previos
- - -
Debemos conocer la siguiente [guía de estilos](https://dart.dev/guides/language/effective-dart/style) para poder programar de forma efectiva en Dart.

### <a name="lesson06"></a> Lección #6 - Variables
- - -
Trabajaremos en el main de nuestro proyecto de la carpeta example. Existen distintos tipos de variables como:
* number (Que pueden ser int o double).
* string
* booleans
* lists
* maps
* runes
* symbols

A lo largo de este curso trabajaremos con todos excepto con runes y symbols. Podemos definir las variables de la siguiente manera: `var idioma;`

O algo muy Java sería de esta forma: `String idioma;`

Hay que tener en cuenta que si usamos var, la variable puede ser de cualquier tipo (muy parecido a Js y a PHP).

### <a name="lesson07"></a> Lección #7 - Números
- - -
* El tipo `num` permite valores enteros y decimales.
* El tipo `int` solo permite valores enteros.
* El tipo `double` también permite valores enteros y decimales pero los expresa todos como números decimales.

### <a name="lesson08"></a> Lección #8 - Operadores
- - -
Uso de operadores sin condicionales. Ejemplo:
```dart
void main(List<String> args) {
  var a = 10;
  var x = 20;

  print(a <= x);
}
```

Los operadores existentes son:
* \==
* \!=
* \<
* \>
* \<=
* \>=

### <a name="lesson09"></a> Lección #9 - Constantes, final y boolean
- - -
* Para definir booleanos usamos el tipo `bool`.
* Las constantes se definen con el tipo `const`.
* El tipo `final` es muy similar al tipo `const` pero la principal diferencia es que `final` es un valor que se puede establecer cuando se compila el programa, `const` puede establecerse un poco después de haberse compilado el programa.

### <a name="lesson10"></a> Lección #10 - Cadenas
- - -
Los elementos que tenemos en dart son objetos, por lo cual podemos hacer uso de métodos para poder interactuar con ellos. Es importante saber que cuando estamos usando el método print, nuestro objeto debe estar denotado de la siguiente forma `${myVar}` para poder acceder a todos los métodos que tenemos presentes... De lo contrario, solo tendríamos las expresiones. Ejemplo:

```dart
void main(List<String> args) {
  String a = 'Eduardo';
  String b = 'Márquez';
  int x = 10;

  print('$a $b $x');
  print('Hola de nuevo ${a.toUpperCase()}');
}
```

## <a name="module02"></a> Condicionales y ciclos

### <a name="lesson11"></a> Lección #11 - Condicional IF y operadores
- - -
En esta lección trabajaremos con el condicional if de toda la vida. Ejemplo:
```dart
void main(List<String> args) {
  var numero = 1;
  var n = 10;

  if (numero < n) {
    print('numero < n');
  } else {
    print('numero > n');
  }
}
```

Pero existen otro tipo de condicionales como:
* as
* is
* !is

Ejemplo:
```dart
void main(List<String> args) {
  var numero = 1;
  var n = 10;

  if (numero < n) {
    print('numero < n');
  } else {
    print('numero > n');
  }

  print(numero is String);
  print(numero is num);
}
```

También tendremos operadores como && y ||.

### <a name="lesson12"></a> Lección #12 - Ciclo for
- - -
Ejemplo de un ciclo for:
```dart
void main(List<String> args) {
  for (var i = 0; i < 10; i++) {
    print('El valor de i es ${i}');
  }
}
```

### <a name="lesson13"></a> Lección #13 - While y Do While
- - -
Ejemplos de ciclos While y Do While:
```dart
void main(List<String> args) {
  var x = 0;
  while(x < 10) {
    print('El valor de x es ${x}');
    x++;
  }

  var j = 0;
  do {
    print('El valor de j es ${j}');
    j++;
    break;
  } while (j < 10);
}
```

*Nota: También podemos usar el break para romper con las condicionales como en cualquier otro lenguaje de programación.*

### <a name="lesson14"></a> Lección #14 - Switch
- - -
Ejemplo del switch:
```dart
void main(List<String> args) {
  var x = 10;

  switch (x) {
    case 1:
      print('1');
      break;
    case 2:
      print('2');
      break;
    default:
      print('Default ${x}');
  }
}
```

## <a name="module03"></a> Funciones en Dart

### <a name="lesson15"></a> Lección #15 - Funciones
- - -
Ejemplo de funciones en Dart:
```dart
void main(List<String> args) {
  miFuncion();
}

miFuncion() {
  print('Mi función');
  segundaFuncion();
}

segundaFuncion() {
  print('Segunda función');
}
```

### <a name="lesson16"></a> Lección #16 - Retorno de elementos
- - -
La esencia de una función es que retorne valores... Por lo tanto lo que teníamos en la lección anterior estaba incompleto. Para esta lección definiremos lo siguiente:
```dart
void main(List<String> args) {
  print(miFuncion());
}

String miFuncion() {
  return 'Esto es una cadena';
}
```

Cuando estemos retornando valores de las funciones es importante definir el tipo de valor que deseamos retornar. En este caso estamos devolviendo un String y se está definiendo antes del nombre de la función. 

### <a name="lesson17"></a> Lección #17 - Parámetros en funciones
- - -
Lo importante de los parámetros es definir el tipo de valores que podremos recibir. Ejemplo:
```dart
void main(List<String> args) {
  print(miFuncion('Eduardo', 27));
}

String miFuncion(String nombre, int numero) {
  return 'Esto es una cadena';
}
```

Sin embargo, también podemos definir funciones "similares" a las arrow functions de Js... Ejemplo:
```dart
void main(List<String> args) {
  print(miFuncion('Eduardo', 27));
}

String miFuncion(String nombre, int numero) => 'Esto es una cadena $nombre $numero';
```

### <a name="lesson18"></a> Lección #18 - Parámetros opcionales
- - -
Los parámetros opcionales se definen con corchetes de la siguiente manera:
```dart
void main(List<String> args) {
  print(miFuncion('Eduardo'));
}

String miFuncion(String nombre, [int numero]) {
  return 'Esto es una cadena $nombre $numero';
}
```

Pero es importante manejar los casos cuando la variable es opcional y se requerían en un momento dado (como por ejemplo una operación matemática). Ejemplo:
```dart
void main(List<String> args) {
  var x = 10;
  print(operacion(x));
}

int operacion(int x, [int y]) {
  if (y != null) {
    return x + y;
  } else {
    return x;
  }
}
```

## <a name="module04"></a> Programación orientada a objetos

### <a name="lesson19"></a> Lección #19 - Clases
- - -
La clase es la plantilla para definir un objeto. Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;
}

void main(List<String> args) {
  var eduardo = new Persona();
  eduardo.nombre = "Eduardo Márquez";
  eduardo.edad = 27;

  print(eduardo.nombre);
}
```

### <a name="lesson20"></a> Lección #20 - Métodos
- - -
Los métodos son simplemente las funciones o las acciones que hacen los objetos y que se establecen en la clase, se encargan de modificar el estado del objeto. Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;

  void decirNombre() {
    print('Este es mi nombre: ${nombre}');
  }
}

void main(List<String> args) {
  var eduardo = new Persona();
  eduardo.nombre = "Eduardo Márquez";
  eduardo.edad = 27;

  eduardo.decirNombre();
}
```

*Nota: void significa que no retornará valores.*

### <a name="lesson21"></a> Lección #21 - Constructor
- - -
Los métodos constructores son funciones que nos permiten ejecutar instrucciones antes de crear el objeto. Se definen de la siguiente manera:
```dart
class Persona {
  String nombre;
  int edad;

  Persona(String nombre, int edad) {
    this.nombre = nombre;
    this.edad = edad;
  }

  void decirNombre() {
    print('Este es mi nombre: ${nombre}');
  }
}

void main(List<String> args) {
  var eduardo = Persona('Eduardo Márquez', 27);

  eduardo.decirNombre();
}
```

Pero, nosotros podemos simplificar mucha más este código de la siguiente manera:
```dart
class Persona {
  String nombre;
  int edad;

  Persona(this.nombre, this.edad);

  void decirNombre() {
    print('Este es mi nombre: ${nombre}');
  }
}

void main(List<String> args) {
  var eduardo = Persona('Eduardo Márquez', 27);

  eduardo.decirNombre();
}
```

Esto último es algo único de Dart ya que el constructor es usado habitualmente para asignar los valores de las propiedades o atributos.

### <a name="lesson22"></a> Lección #22 - Tipos de constructores
- - -
Existen dos tipos de constructores:
* El *Constructor* de toda la vida.
* El *Named Constructor* que es el constructor de la clase pero que ejecutará otro tipo de función.

Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;

  Persona(this.nombre, this.edad);

  Persona.loquesea(this.nombre) {
    this.nombre = "Eduardo Joven";
    this.edad = 21;
  }

  void decirNombre() {
    print('Este es mi nombre: ${nombre}');
  }
}

void main(List<String> args) {
  var eduardo = Persona('Eduardo Márquez', 27);
  var x = Persona.loquesea('Javier');

  eduardo.decirNombre();
  x.decirNombre();
}
``` 

### <a name="lesson23"></a> Lección #23 - Set y Get
- - -
Ejemplo:
```dart
class Persona {
  String nombre;
  String get getNombre => nombre;
  set setNombre(String valor) => nombre = valor;

  int edad;

  Persona(this.nombre, this.edad);

  Persona.loquesea(this.nombre) {
    this.nombre = "Eduardo Joven";
    this.edad = 21;
  }

  void decirNombre() {
    print('Este es mi nombre: ${nombre}');
  }
}

void main(List<String> args) {
  var eduardo = Persona('Eduardo Márquez', 27);
  eduardo.setNombre = 'Nuevo Nombre';
  var x = Persona.loquesea('Javier');

  eduardo.decirNombre();
  x.decirNombre();
}
```

### <a name="lesson24"></a> Lección #24 - Herencia en Dart
- - -
Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;
}

class Hombre extends Persona {
  var altura = 1.90;
}

class Mujer extends Persona {

}

void main(List<String> args) {
  var eduardo = Hombre();
  eduardo.nombre = 'Eduardo';
  print(eduardo.nombre);
  print(eduardo.altura);
}
```

### <a name="lesson25"></a> Lección #25 - Sobre escribir métodos
- - -
Para sobreescribir métodos heredados de otras clases lo haremos con la palabra reservada `@override`. Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;

  void sumarEdad() {
    this.edad = 10;
  }
}

class Hombre extends Persona {
  var altura = 1.90;

  @override
  void sumarEdad() {
    this.edad = 0;
  }
}

class Mujer extends Persona {

}

void main(List<String> args) {
  var eduardo = Hombre();
  eduardo.nombre = 'Eduardo';
  print(eduardo.nombre);
  print(eduardo.altura);
  eduardo.sumarEdad();
  print(eduardo.edad);
}
```

### <a name="lesson26"></a> Lección #26 - Herencia y constructor
- - -
En dart, la herencia y el constructor no funciona exactamente como en Java o JavaScript. Se tiene establecida una forma diferente donde debemos recibir los parámetros y pasarlos a la clase superior mediante la palabra reservada super. Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;

  void sumarEdad() {
    this.edad = 10;
  }

  Persona(this.nombre, this.edad);
}

class Hombre extends Persona {
  var altura = 1.90;

  Hombre(String nombre, int edad) : super(nombre, edad);

  @override
  void sumarEdad() {
    this.edad = 0;
  }
}

class Mujer extends Persona {
  Mujer(String nombre, int edad) : super(nombre, edad);
}

void main(List<String> args) {
  var eduardo = Hombre('Eduardo Márquez', 27);
  print(eduardo.nombre);
  print(eduardo.altura);
  eduardo.sumarEdad();
  print(eduardo.edad);
}
```

### <a name="lesson27"></a> Lección #27 - Clases abstractas
- - -
Una clase que declara la existencia de métodos pero no la implementación de dichos métodos (o sea, las llaves { } y las sentencias entre ellas), se considera una clase abstracta. Estos métodos sin definición también pueden conocerse como **métodos abstractos**. En esta lección definiremos una clase abstracta llamada figura donde estarán los métodos de calcular el area y el perimetro puesto que no tiene la misma lógica cuando calculamos el area de un circulo contra un cuadrado. 

*Nota: Las clases abstractas no nos permiten crear objetos.*

Ejemplo:
```dart
abstract class Figura {
  void calcularArea();

  void calcularPerimetro();

  int operacion() {
    return 2;
  }
}

class Triangulo extends Figura {
  @override
  void calcularArea() {
    // TODO: implement calcularArea
  }

  @override
  void calcularPerimetro() {
    // TODO: implement calcularPerimetro
  }
}

void main(List<String> args) {
  var triangulo = new Triangulo();
  print(triangulo.operacion());
}
```

### <a name="lesson28"></a> Lección #28 - Interfaces
- - -
En Dart no existen las interfaces... Todo se define en clases abstractas... En dart las clases **traen de manera implicita** una interfaz.

**Las características de una interfaz** no cambian a pesar de que no exista en dart, es decir, que podemos tener métodos definidos y métodos abstractos. **Una diferencia bien marcada en dart** es que podemos definir una clase abstracta sin un método abstracto (**Esto en otros lenguajes de programación no es posible**).

**¿Cuándo vamos a notar la diferencia?** Cuando usemos la palabra reservada `implements` en lugar de `extends`. Ejemplo:
```dart
abstract class Figura {
  void calcularArea();

  void calcularPerimetro();

  int operacion() {
    return 2;
  }
}

abstract class MiInterface {
  void sumar();
  void operacion() {
    print('Operación');
  }
}

class Triangulo implements Figura {
  @override
  void calcularArea() {
    // TODO: implement calcularArea
  }

  @override
  void calcularPerimetro() {
    // TODO: implement calcularPerimetro
  }

  @override
  int operacion() {
    // TODO: implement operacion
    return 10;
  }
}

void main(List<String> args) {
  var triangulo = new Triangulo();
  print(triangulo.operacion());
}
```

De la lección anterior, si implementamos en lugar de extender la clase, nos pide el IDE que debemos hacer un override de los tres métodos, de lo contrario el código no funcionaría. En cambio con extends solo debemos hacer override de los métodos abstractos.

## <a name="module05"></a> Estructura de datos e imports

### <a name="lesson29"></a> Lección #29 - Listas
- - -
En las primeras clases hicimos lo siguiente:
```dart
void main(List<String> args) {  }
```

Esto se refiere a una lista de cadenas con el nombre de `args` (El nombre pudo haber sido cualquiera), en otros lenguajes de programación se le conoce como arreglos o arrays. Ejemplo:
```dart
void main(List<String> args) {
  var list = [10, 9, 2, 5, 6, 7];
  print(list[0]);

  for (var i = 0; i < list.length; i++) {
    print('Indice ${i} elemento ${list[i]}');
  }
}
```

### <a name="lesson30"></a> Lección #30 - Listas de objetos
- - -
Crearemos una lista de objetos. Ejemplo:
```dart
class Persona {
  String nombre;
  int edad;

  Persona(this.nombre);
}

void main(List<String> args) {
  var eduardo = Persona('Eduardo');
  var maria = Persona('Maria');
  var jose = Persona('Jose');

  var list = <Persona>[];
  list.add(eduardo);
  list.add(maria);
  list.add(jose);

  for (var i = 0; i < list.length; i++) {
    print(list[i].nombre);
  }
}
```

### <a name="lesson31"></a> Lección #31 - Maps
- - -
Los maps son muy parecidos a las listas, solo que tenemos dos valores que son el **key** (O identificador) y el **valor**. Son muy parecidos a los objetos JSON. Ejemplo:
```dart
void main(List<String> args) {
  var persona = {
    'nombre': 'Eduardo',
    'apellido': 'Márquez',
    'edad': 27,
    1: 'Lo que sea'
  };

  print(persona['nombre']);

  persona.forEach((key, value) => print(value));
  
  var keys = persona.keys;
  print(keys);

  var values = persona.values;
  print(values);
}
```

### <a name="lesson32"></a> Lección #32 - Imports
- - -
En esta última lección, hablaremos del import que se genera en el scaffolding de este proyecto. Ejemplo:
```dart
import 'package:curso_del_lenguaje_dart/curso_del_lenguaje_dart.dart';

void main(List<String> args) {
  var awesome = Awesome();
  print(awesome.isAwesome);
}
```

Al igual que en otros lenguajes de programación, existen muchas librerías que nos ayudan a simplificar el trabajo en Dart. Así que debemos estar atentos antes de no reinventar la rueda cuando se esté desarrollando una aplicación.